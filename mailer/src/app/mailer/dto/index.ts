import {
  MailingDtoForm,
  SMS_TYPE,
  EMAIL_TYPE,
  SmsBody,
  EmailBody,
} from './mailer.mailingDto';

export {
  MailingDtoForm,
  SMS_TYPE,
  EMAIL_TYPE,
  SmsBody,
  EmailBody,
};
