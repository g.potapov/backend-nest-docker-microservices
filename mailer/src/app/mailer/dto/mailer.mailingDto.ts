import {
  IsString,
  ValidateNested,
  IsOptional,
  IsJSON,
  ValidateIf,
  IsIn,
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
} from 'class-validator';

import { Type } from 'class-transformer';
import { IsUrlArray, manualValidator } from '../infrastructure/common/customValidateDecorators';
import { Json } from '../infrastructure/common/types';

const EMAIL_TYPE = 'email';
const SMS_TYPE = 'sms';

function RecipientsByChannelType(property: string, validateOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) => {
    registerDecorator({
      name: 'recipientsByChannelType',
      target: object.constructor,
      propertyName,
      options: validateOptions,
      constraints: [property],
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];

          let result: boolean = Array.isArray(value) && value.length > 0;

          switch (relatedValue) {
            case EMAIL_TYPE:
              result = result && value.every((elem: string) => manualValidator.isEmail(elem));
              break;

            case SMS_TYPE:
              result = result && value.every((elem: string) => manualValidator.isMobilePhone(elem, 'ru-RU'));
              break;
          }

          return result;
        },
      },
    });
  };
}

abstract class MailingBody {}

class EmailBody extends MailingBody {
  @IsJSON()
  renderArgs!: Json;

  @IsString()
  subject!: string;
}

class SmsBody extends MailingBody {
  @IsString()
  text!: string;
}

class MailingDtoForm {
  @IsIn([EMAIL_TYPE, SMS_TYPE], {
    message: 'need channel, one of: sms, email',
  })
  channel!: string;

  @ValidateIf((obj) => !!obj.channel)
  @RecipientsByChannelType('channel', {
    message: (args: ValidationArguments) => {
      const [channel] = args.constraints;
      let result;


      if (channel === EMAIL_TYPE) {
        result = 'each element need email format';
      } else {
        result = 'each element need mobile phone format';
      }

      return result;
    },
  })
  recipients!: string[];

  @Type(() => MailingBody, {
    discriminator: {
      property: '__type',
      subTypes: [
        { value: SmsBody, name: 'smsBody' },
        { value: EmailBody, name: 'emailbody' },
      ],
    },
  })
  @ValidateNested()
  body!: EmailBody | SmsBody;

  @IsOptional()
  @ValidateIf((obj) => obj.channel === EMAIL_TYPE)
  @IsUrlArray({
    message: 'each item need like url format',
  })
  attachmentsUrls!: string[];
}

export {
  MailingDtoForm,
  EMAIL_TYPE,
  SMS_TYPE,
  EmailBody,
  SmsBody,
};
