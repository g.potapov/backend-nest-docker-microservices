import { readFileSync } from 'fs';
import { join } from 'path';

abstract class ServiceDiscovery {
  public abstract getService(serivce: string): string;
}

class EnvServiceDiscovery extends ServiceDiscovery {
  // eslint disable-next-line class-methods-use-this
  public getService(serivce: string) {
    return process.env[`${serivce.toUpperCase()}_SERVICE`] as string;
  }
}

class JsonServiceDiscovery extends ServiceDiscovery {
  protected static PATH_TO_SERVICES = join(process.cwd(), 'services.json');

  protected services: Map<string, string>;

  constructor() {
    super();
    this.services = new Map(
      Object.entries(JSON.parse(readFileSync(JsonServiceDiscovery.PATH_TO_SERVICES, { encoding: 'utf8' }))),
    );
  }

  public getService(serivce: string) {
    return this.services.get(`${serivce.toUpperCase()}_SERVICE`) as string;
  }
}

export {
  ServiceDiscovery,
  EnvServiceDiscovery,
  JsonServiceDiscovery,
};
