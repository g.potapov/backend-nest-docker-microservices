import { readFileSync } from 'fs';
import { join } from 'path';
import { validateSync, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';

import { Json, ENV_TYPES } from './types';
import { DevConfigClass, ProdConfigClass } from '../configs/validateClasses';

const DEFAULT_CONFIG_DIERCTORY = 'config';
const LOCAL_CONFIG_DIRECTORY = 'local';
const DEFAULT_LOCAL_CONFIG_NAME = 'local.json';
const LOCAL_CONFIG_PATH = join(process.cwd(),
  DEFAULT_CONFIG_DIERCTORY,
  LOCAL_CONFIG_DIRECTORY,
  DEFAULT_LOCAL_CONFIG_NAME);

class ConfigValidationError extends Error {
  protected reason: ValidationError[];

  constructor(reason: ValidationError[]) {
    super();
    this.reason = reason;
  }
}

class ConfigLoader {
  protected directoryPath: string;

  protected envVar: ENV_TYPES.DEV | ENV_TYPES.PROD;

  constructor() {
    this.envVar = process.env.BACKEND_ENV as ENV_TYPES.DEV | ENV_TYPES.PROD;
    this.directoryPath = join(process.cwd(), DEFAULT_CONFIG_DIERCTORY, this.envVar);
  }

  protected internalLoad() {
    const
      byEnvConfig = JSON.parse(
        readFileSync(
          join(this.directoryPath, `${this.envVar}.json`),
          { encoding: 'utf8' },
        ),
      );

    const localConfig = ConfigLoader.getLocalConfig();

    return localConfig
      ? {
        byEnvConfig,
        ...localConfig,
      }
      : byEnvConfig;
  }

  protected static getLocalConfig() {
    let config;
    try {
      config = JSON.parse(
        readFileSync(LOCAL_CONFIG_PATH, { encoding: 'utf8' }),
      );
    } catch (e) {
      // TODO add error handling, logging, etc
    }

    return config;
  }

  protected validate(config: Json) {
    let errors = [] as ValidationError[];

    if (this.envVar === ENV_TYPES.DEV) {
      errors = validateSync(plainToClass(DevConfigClass, config));
    } else if (this.envVar === ENV_TYPES.PROD) {
      errors = validateSync(plainToClass(ProdConfigClass, config));
    }

    if (errors.length) {
      // TODO add error parser for human readable view
      throw new ConfigValidationError(errors);
    }

    return config;
  }

  public load() {
    return this.validate(this.internalLoad());
  }
}

async function loadConfig() {
  return new ConfigLoader().load();
}

export {
  loadConfig,
  ConfigLoader,
};
