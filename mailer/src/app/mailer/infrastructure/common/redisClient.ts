import { ConfigService } from '@nestjs/config';
import * as IORedis from 'ioredis';
import { join } from 'path';

import { RedisConfig } from './configs';

const LUA_FOLDER = join('dist', 'infrastructure', 'luaScripts');
const LUA_SCRIPTS_PATH = join(process.cwd(), LUA_FOLDER);

class RedisClient {
  protected readonly nativeClient: IORedis.Redis;

  protected readonly configService: ConfigService;

  constructor(configService: ConfigService) {
    this.configService = configService;
    this.nativeClient = new IORedis(configService.get<RedisConfig>('redis'));
  }

  public get native() {
    return this.nativeClient;
  }
}

export {
  RedisClient,
  LUA_SCRIPTS_PATH,
};
