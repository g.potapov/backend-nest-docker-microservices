import {
  IsNumber, Min, Max, IsString,
} from 'class-validator';
import { IsHostOrIP } from './customValidateDecorators';

class DbConfig {
  @IsString()
  host!: string;

  @IsNumber()
  @Min(0)
  @Max(65535)
  port!: number;

  @IsString()
  databaseName!: string;

  @IsString()
  login!: string;

  @IsString()
  password!: string;
}

class RedisConfig {
  @IsNumber()
  @Min(0)
  @Max(65535)
  port!: number;

  @IsString()
  host!: string;

  // @IsString()
  // password!: string;

  @IsNumber()
  @Min(0)
  @Max(15)
  db!: number;
}

class SmtpConfig {
  @IsHostOrIP()
  host!: string;

  @IsString()
  login!: string;

  @IsString()
  password!: string;

  @IsNumber()
  @Min(0)
  @Max(65535)
  port!: number;

  @IsString()
  from!: string;
}

export {
  DbConfig,
  RedisConfig,
  SmtpConfig,
};
