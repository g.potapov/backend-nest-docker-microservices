type ResolverType<TResult> = (result?: TResult | PromiseLike<TResult>) => void;
type RejectorType = (result?: Error) => void;

class Task<TResult> {
  protected isFullfiled: boolean;

  protected isResolve: boolean;

  protected promiseInstance: Promise<TResult>;

  public resolver: ResolverType<TResult>;

  public rejector: RejectorType;

  constructor() {
    this.isResolve = false;
    this.isFullfiled = false;

    let resolver: ResolverType<TResult> | undefined;
    let rejector: RejectorType | undefined;

    this.promiseInstance = new Promise<TResult>((resolve, reject) => {
      resolver = resolve;
      rejector = reject;
    });
    this.resolver = resolver!;
    this.rejector = rejector!;
  }

  public resolve(result?: TResult) {
    if (!this.isResolve) {
      this.isResolve = true;
      this.resolve(result);
    }
  }

  public reject(result?: Error) {
    if (!this.isResolve) {
      this.isResolve = true;
      this.reject(result);
    }
  }

  public get promise() {
    return this.promiseInstance;
  }
}

// only use in callback last argument pattern
function promisify<TResult>(sourceFunction: Function) {
  const task = new Task<TResult>();

  return (...args: any[]): Promise<TResult> => {
    const callback = (error: Error, result: TResult): void => {
      if (error) {
        task.reject(error);
        return;
      }
      task.resolve(result);
    };

    args.push(callback);
    sourceFunction(...args);

    return task.promise;
  };
}

export {
  Task,
  promisify,
};
