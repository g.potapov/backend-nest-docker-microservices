import { EnvServiceDiscovery } from './serviceDiscovery';

import needle = require('needle');

// TODO add base infrastucture class error, replace to inherit!
class RestApiError extends Error {
  protected readonly reason: Error;

  constructor(reason: Error) {
    super();
    this.reason = reason;
  }
}

// FIXME add additional methods if needed
class RestClient {
  protected readonly servicePath: string;

  constructor(serviceName: string) {
    this.servicePath = new EnvServiceDiscovery().getService(serviceName);
  }

  public async get(uri?: string, requestOptions?: needle.NeedleOptions): Promise<any> | never {
    try {
      return (await needle('get', `${this.servicePath}/${uri}`, requestOptions)).body;
    } catch (err) {
      RestClient.handleError(err);
    }
  }

  public async post(uri: string | undefined, body: needle.BodyData, requestOptions?: needle.NeedleOptions): Promise<any> | never {
    try {
      return (await needle('post', this.formatLink(uri), body, requestOptions)).body;
    } catch (err) {
      RestClient.handleError(err);
    }
  }

  public async put(uri: string | undefined, body: needle.BodyData, requestOptions?: needle.NeedleOptions): Promise<any> | never {
    try {
      return (await needle('put', this.formatLink(uri), body, requestOptions)).body;
    } catch (err) {
      RestClient.handleError(err);
    }
  }

  public async patch(uri: string | undefined,
    body: needle.BodyData, requestOptions?: needle.NeedleOptions): Promise<any> | never {
    try {
      return (await needle('patch', this.formatLink(uri), body, requestOptions)).body;
    } catch (err) {
      RestClient.handleError(err);
    }
  }

  public async delete(uri: string | undefined,
    body: needle.BodyData, requestOptions?: needle.NeedleOptions): Promise<any> | never {
    try {
      return (await needle('delete', this.formatLink(uri), body, requestOptions)).body;
    } catch (err) {
      RestClient.handleError(err);
    }
  }

  protected formatLink(uri?: string) {
    return uri
      ? `${this.servicePath}/${uri}`
      : this.servicePath;
  }

  protected static handleError(err: Error) {
    // TODO add logging
    // eslint-disable-next-line no-console
    console.log('error thougth request: ', err);
    throw new RestApiError(err);
  }
}

export {
  RestClient,
  RestApiError,
};
