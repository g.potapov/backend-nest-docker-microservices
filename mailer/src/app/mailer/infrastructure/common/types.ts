type Json =
    | string
    | number
    | boolean
    | null
    | { [key: string]: Json }
    | Json[];

type Nullable<T> = T | null;
type Undefable<T> = T | undefined;

enum ENV_TYPES {
  PROD = 'prod',
  DEV = 'dev',
}

export {
  Json,
  Nullable,
  Undefable,
  ENV_TYPES,
};
