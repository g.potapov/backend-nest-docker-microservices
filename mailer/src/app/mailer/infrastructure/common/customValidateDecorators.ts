import { registerDecorator, Validator, ValidationOptions } from 'class-validator';

const validator = new Validator();

function IsBuffer(validateOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) => {
    registerDecorator({
      name: 'isBuffer',
      target: object.constructor,
      propertyName,
      options: validateOptions,
      validator: {
        validate(value: any) {
          return Buffer.isBuffer(value);
        },
      },
    });
  };
}

function IsEmailArray(validateOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) => {
    registerDecorator({
      name: 'isEmailArray',
      target: object.constructor,
      propertyName,
      options: validateOptions,
      validator: {
        validate(value: any) {
          return Array.isArray(value)
                        && value.length > 0
                        && value.every((elem: string) => validator.isEmail(elem));
        },
      },
    });
  };
}

function IsUrlArray(validateOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) => {
    registerDecorator({
      name: 'isUrlArray',
      target: object.constructor,
      propertyName,
      options: validateOptions,
      validator: {
        validate(value: any) {
          return Array.isArray(value)
                        && value.length > 0
                        && value.every((elem: string) => validator.isURL(elem));
        },
      },
    });
  };
}

function IsHostOrIP(validateOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) => {
    registerDecorator({
      name: 'isHostOrIP',
      target: object.constructor,
      propertyName,
      options: validateOptions,
      validator: {
        validate(value: any) {
          return validator.isURL(value) || validator.isIP(value);
        },
      },
    });
  };
}

export {
  IsBuffer,
  IsEmailArray,
  IsUrlArray,
  IsHostOrIP,
  validator as manualValidator,
};
