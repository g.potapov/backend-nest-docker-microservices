class ApplicationError extends Error {
  protected reason: Error;

  constructor(reason: Error) {
    super('Application error');
    this.reason = reason;
  }
}

export {
  ApplicationError,
};
