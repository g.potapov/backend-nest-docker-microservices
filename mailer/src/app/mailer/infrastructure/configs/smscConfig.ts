import {
  IsString, ValidateNested, IsMobilePhone, IsNumber,
} from 'class-validator';

class AuthOptions {
  @IsString()
  login!: string;

  @IsString()
  password!: string;
}

class SmscConfig {
  @ValidateNested()
  auth!: AuthOptions;

  @IsMobilePhone('ru-RU', { each: true })
  noticeNumbers!: string[];

  @IsNumber()
  minimalBalanceToNotification!: number;
}

export {
  SmscConfig,
};
