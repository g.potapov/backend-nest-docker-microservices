import {
  DevConfigClass,
} from './dev';

import {
  ProdConfigClass,
} from './prod';

export {
  DevConfigClass,
  ProdConfigClass,
};
