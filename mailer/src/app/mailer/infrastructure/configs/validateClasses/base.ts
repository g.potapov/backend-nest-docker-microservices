import { ValidateNested, IsDefined } from 'class-validator';

import { Type } from 'class-transformer/decorators';
import { RedisConfig, SmtpConfig } from '../../common/configs';
import { SmscConfig } from '../smscConfig';

class BaseConfigClass {
  @IsDefined()
  @ValidateNested()
  @Type(() => RedisConfig)
  redis!: RedisConfig;

  @IsDefined()
  @ValidateNested()
  @Type(() => SmtpConfig)
  smtp!: SmtpConfig;

  @IsDefined()
  @ValidateNested()
  @Type(() => SmscConfig)
  smsc!: SmscConfig;
}

export {
  BaseConfigClass,
};
