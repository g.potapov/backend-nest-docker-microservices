import {
  Controller,
  Post,
  Body,
  Inject,
  ValidationPipe,
} from '@nestjs/common';

import { MailerService, EmailBoundary, SmsBoundary } from '../providers/mailer.service';
import {
  MailingDtoForm,
  EMAIL_TYPE,
  SmsBody,
  EmailBody,
} from '../dto';
import { SmsApi, EmailApi, RenderServiceApi } from '../components';
import { Sms, Email } from '../entities/mailer.domainEntity';
import { Json } from '../infrastructure/common/types';
import { Type } from '../../../types';

@Controller('/send')
class MailerController {
  // eslint disable-next-line no-useless-constructor
  constructor(
    private readonly mailerService: MailerService,
    @Inject(Type.SmsApi)private readonly smsApi: SmsApi,
    private readonly emailApi: EmailApi,
    @Inject(Type.RenderServiceApi)private readonly renderApi: RenderServiceApi,
  ) {}

  @Post()
  async sendMailing(@Body(new ValidationPipe({
    transform: true,
  })) mailingData: MailingDtoForm): Promise<void> {
    let boundary: EmailBoundary | SmsBoundary = {
      getRequisites: () => ({ recipients: mailingData.recipients }),
      getText: () => (mailingData.body as SmsBody).text,
      sendSms: (body: Sms) => this.smsApi.sendSms({
        phones: body.requisites.recipients,
        mes: body.text,
      }),
    };

    if (mailingData.channel === EMAIL_TYPE) {
      boundary = {
        getRequisites: () => ({
          recipients: mailingData.recipients,
          subject: (mailingData.body as EmailBody).subject,
        }),
        getAttachments: () => {
          let attachments: { filename: string; path: string }[] = [];

          if (mailingData.attachmentsUrls) {
            attachments = mailingData.attachmentsUrls
              .map((link: string) => ({
                filename: MailerController.getAttachName(link),
                path: link,
              }));
          }
          return attachments;
        },
        renderHtml: () => this.renderApi.render<Json>((mailingData.body as EmailBody).renderArgs),
        sendEmail: (email: Email) => this.emailApi.sendEmail({
          html: email.htmlBody,
          to: email.requisites.recipients,
          subject: email.requisites.subject,
          attachments: email.attachments || undefined,
        }),
      };
    }

    await this.mailerService.send(boundary);
  }

  protected static getAttachName(staticLink: string) {
    return staticLink.split('/').reverse()[0];
  }
}

export {
  MailerController,
};
