import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { MailerController } from './controllers/mailer.controller';
import { MailerService } from './providers/mailer.service';
import { RedisClient } from './infrastructure/common/redisClient';
import { SmsApi } from './components/sendSms';
import { EmailApi } from './components/sendEmail';
import { RenderServiceApi } from './components/renderApi';
import { RestClient } from './infrastructure/common/restClient';
import { Type } from '../../types';

@Module({
  imports: [ConfigModule],
  controllers: [MailerController],
  providers: [
    MailerService,
    EmailApi,
    {
      provide: Type.RenderServiceApi,
      useValue: new RenderServiceApi(new RestClient('mock_render')),
    },
    {
      provide: Type.SmsApi,
      useFactory: async (configService: ConfigService) => {
        const smsApi = new SmsApi(configService, new RedisClient(configService));
        await smsApi.init();
        return smsApi;
      },
      inject: [ConfigService],
    },
  ],

})
export class MailerModule {}
