import { Injectable } from '@nestjs/common';
import * as util from 'util';

import { Email, Sms } from '../entities/mailer.domainEntity';
import { ApplicationError } from '../infrastructure/errors/ApplicationError';

interface EmailBoundary {
  getRequisites: () => { recipients: string[]; subject: string };
  getAttachments: () => { filename: string; path: string }[] | undefined;
  renderHtml: () => Promise<string>;
  sendEmail: (body: Email) => Promise<void>;
}

interface SmsBoundary {
  getRequisites: () => { recipients: string[] };
  getText: () => string;
  sendSms: (body: Sms) => Promise<void | Error>;
}

const isEmailBoundary = (obj: EmailBoundary | SmsBoundary): obj is EmailBoundary => 'sendEmail' in obj;
const isSmsBoundary = (obj: EmailBoundary | SmsBoundary): obj is SmsBoundary => 'sendSms' in obj;

@Injectable()
class MailerService {
  public async send(boundary: EmailBoundary | SmsBoundary): Promise<void> {
    try {
      if (isEmailBoundary(boundary)) {
        const {
          getRequisites, getAttachments, renderHtml, sendEmail,
        } = boundary;

        const email: Email = {
          requisites: getRequisites(),
          attachments: getAttachments(),
          htmlBody: await renderHtml(),
        };

        await sendEmail(email);
      }

      if (isSmsBoundary(boundary)) {
        const { getRequisites, sendSms, getText } = boundary;

        const sms: Sms = {
          requisites: getRequisites(),
          text: getText(),
        };

        await sendSms(sms);
      }
    } catch (err) {
      // TODO add logging
      // eslint-disable-next-line no-console
      console.log(`Application error, 
        ${util.inspect(err, { depth: null, compact: true })}`);
      throw new ApplicationError(err);
    }
  }
}

export {
  EmailBoundary,
  SmsBoundary,
  MailerService,
};
