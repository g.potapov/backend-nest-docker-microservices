interface Email {
  htmlBody: string;
  attachments?: { filename: string; path: string }[];
  requisites: { recipients: string[]; subject: string };
}

interface Sms {
  text: string;
  requisites: { recipients: string[] };
}

export {
  Email,
  Sms,
};
