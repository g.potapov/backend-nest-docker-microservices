import { Injectable } from '@nestjs/common';
import { RestClient } from '../infrastructure/common/restClient';

class RenderClientApiError extends Error {
  protected readonly reason: Error;

  constructor(reason: Error) {
    super();
    this.reason = reason;
  }
}

@Injectable()
class RenderServiceApi {
  protected readonly restClient: RestClient;

  constructor(restClient: RestClient) {
    this.restClient = restClient;
  }

  public async render<TRenderSArgs>(renderArgs: TRenderSArgs) {
    try {
      return await this.restClient.post(undefined, renderArgs, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } catch (err) {
      throw new RenderClientApiError(err);
    }
  }
}

export {
  RenderClientApiError,
  RenderServiceApi,
};
