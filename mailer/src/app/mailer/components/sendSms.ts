import { ConfigService } from '@nestjs/config';

import { Injectable } from '@nestjs/common';
import { RedisClient } from '../infrastructure/common/redisClient';
import { SmscConfig } from '../infrastructure/configs/smscConfig';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const smsc = require('../infrastructure/external_api/smscApi');

interface SendSmsParams {
  phones: string[];
  mes: string;
}

class SmscApiError extends Error {
  protected reason: Error | string;

  constructor(reason: Error | string) {
    super();
    this.reason = reason;
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
@Injectable()
class SmsApi {
  protected configuredSmsc: any;

  protected static NOTICE_TIME_INTERVAL_MS = 2 * 60 * 60 * 1000;

  protected static LAST_NOTICE_DATE_KEY = 'mailer_lastNoticeDate';

  protected lastNoticeDateCache?: string;

  constructor(
    protected readonly configService: ConfigService,
    protected readonly redisClient: RedisClient,
  ) {
    this.configuredSmsc = smsc;
  }

  public async init() {
    this.lastNoticeDateCache = await this.redisClient.native
      .get(SmsApi.LAST_NOTICE_DATE_KEY) ?? undefined;

    await this.configure();
  }

  protected testSmsc() {
    return new Promise<void | SmscApiError>((resolve, reject) => {
      this.configuredSmsc.test((err: string) => {
        if (err) {
          // eslint-disable-next-line no-console
          console.log('error througth test connection to smsc server, reason', err);
          reject(new SmscApiError(err));
          return;
        }
        // eslint-disable-next-line no-console
        console.log('connection to smsc server successfully tested');
        resolve();
      });
    });
  }

  protected async configure() {
    try {
      const configData = this.configService.get<SmscConfig>('smsc.auth');
      this.configuredSmsc.configure(configData);
      await this.testSmsc();
    } catch (err) {
      // TODO add logging
      // eslint-disable-next-line no-console
      console.log(`smsc authorization error: ${err}`);
      throw new SmscApiError(err);
    }
  }

  protected async internalSendSms(data: SendSmsParams) {
    return new Promise<void | SmscApiError>((resolve, reject) => {
      this.configuredSmsc.send_sms({
        ...data,
        cost: 0,
      }, // eslint-disable-next-line @typescript-eslint/no-unused-vars
      (_a: any, _b: any, err: string, _: null) => {
        if (err) {
          // TODO logging
          // eslint-disable-next-line no-console
          console.log(`error througth send sms on numbers ${data.phones.join()}, 
                        reason: ${err}`);
          reject(new SmscApiError(err));
          return;
        }
        resolve();
      });
    });
  }

  protected async getBalance() {
    return new Promise<SmscApiError | number>((resolve, reject) => {
      this.configuredSmsc.get_balance((balance: number, _: any, err: string, code: string) => {
        if (err) {
          // TODO logging
          // eslint-disable-next-line no-console
          console.log(`error througth get balance, reason: ${err}, 
                      code: ${code}`);
          reject(new SmscApiError(err));
          return;
        }
        resolve(balance);
      });
    });
  }

  protected async afterSending() {
    const
      noticeNumbers = this.configService.get<string[]>('smsc.noticeNumbers');
    const minimalBalanceToNotification = this.configService
      .get<string>('smsc.minimalBalanceToNotification');
    const balance = await this.getBalance();

    if (balance < Number(minimalBalanceToNotification)) {
      await this.internalSendSms({
        phones: noticeNumbers || [],
        mes: `на счету сервиса смс оповещения кончаются средства. текущий баланс: ${balance}.`,
      });
      await this.updateLastNotificationDate();
    }
  }

  protected async updateLastNotificationDate() {
    if (!this.lastNoticeDateCache
        || (Date.now() - Date.parse(this.lastNoticeDateCache) > SmsApi.NOTICE_TIME_INTERVAL_MS)) {
      this.lastNoticeDateCache = await this.redisClient
        .native.getset(SmsApi.LAST_NOTICE_DATE_KEY,
          new Date().toISOString()) ?? undefined;
    }
  }

  public async sendSms(data: SendSmsParams) {
    return this.internalSendSms(data)
      .finally(async () => { await this.afterSending.call(this); });
  }
}

export {
  SendSmsParams,
  SmscApiError,
  SmsApi,
};
