import { ConfigService } from '@nestjs/config';
import * as nodemailer from 'nodemailer';
import { Injectable } from '@nestjs/common';

interface EmailSendParams {
  to: string[];
  subject: string;
  html: string;
  attachments?: { filename: string; path: string }[];
}

class EmailApiError extends Error {
  protected reason: Error;

  constructor(reason: Error) {
    super();
    this.reason = reason;
  }
}

@Injectable()
class EmailApi {
  protected transporter: nodemailer.Transporter;

  constructor(protected readonly configService: ConfigService) {
    this.transporter = nodemailer.createTransport(
      {
        host: configService.get<string>('smtp.host'),
        port: configService.get<number>('smtp.port'),
        secure: true,
        auth: {
          user: configService.get<string>('smtp.login'),
          pass: configService.get<string>('smtp.password'),
        },
      },
      {
        from: configService.get<string>('smtp.from'),
      },
    );
  }

  public async sendEmail(emailData: EmailSendParams) {
    try {
      await this.transporter.sendMail(emailData);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`error througth sending email,
                reason: ${err.message}`);
      throw new EmailApiError(err);
    }
  }
}

export {
  EmailApi,
  EmailSendParams,
  EmailApiError,
};
