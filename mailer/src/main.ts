import { NestFactory } from '@nestjs/core';
import { inspect } from 'util';
import { AppModule } from './app.module';


const PORT = 29265;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(PORT);
}

process.on('uncaughtException', (err: Error) => {
  // eslint-disable-next-line no-console
  console.log('uncaught Exception!\n');
  // eslint-disable-next-line no-console
  console.log(inspect(err, { compact: true, depth: null }));
});
process.on('unhandledRejection', (_: any, promise: Promise<any>) => {
  // eslint-disable-next-line no-console
  console.log('unhandled rejection!\n');
  // eslint-disable-next-line no-console
  console.log(inspect(promise, { compact: true, depth: null }));
});

bootstrap();
