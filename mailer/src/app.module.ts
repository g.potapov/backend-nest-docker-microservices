import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { MailerModule } from './app/mailer/mailer.module';
import { loadConfig } from './app/mailer/infrastructure/common/configValidator';

@Module({
  imports: [
    MailerModule,
    ConfigModule.forRoot({
      load: [loadConfig],
    }),
  ],
})
export class AppModule {}
