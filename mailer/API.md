# email JSON fileUrls API
    POST localhost:29265/send

    REQUIRED PARAMS: channel, recipients, body

    JSON
    {
        "channel": "email",
        "recipients": [ "g.potapov@medpoint24.ru" ],
        "body": {
            "subject": "test message",              /// Subject
            "renderArgs": {                        /// ...renderParams
            	"title": "Title!!",
            	"message": "Hello world!"
            }
        },
        "attachmentsUrls": [
            "https://learn.javascript.ru/article/promise/promiseInit.png",
            "https://learn.javascript.ru/article/promise/promiseUserFlow.png"
        ]
    }

# sms JSON API
    POST localhost:29265/send

    REQUIRED PARAMS: channel, recipietns, body

    JSON
    {
        "channel": "sms",
        "recipietns": [ "7XXXXXXXXXX" ],
        "body": {
            "text": "Text of test sms!"
        }
    }

## Tests
    source environment/compose/dev.sh mailer && 
    docker-compose build &&
    docker-compose up -d &&
```
check sms api:

    echo -e '{ "channel": "sms", "recipients": ["89168816801"], "body": {"text": "тестовое смс", "__type": "smsBody" } }' | curl -X POST -H "Content-Type: application/json" -d @- http://localhost:29265/send
```
```
check email api:
    
    echo -e '{ "channel": "email", "recipients": ["georgiy.potapov@gmail.com"], "body": {"renderArgs": { "title": "test title", "someText": "some text here" }, "subject": "test subject", "__type": "emailBody" } }' | curl -X POST -H "Content-Type: application/json" -d @- http://localhost:29265/send
``` 