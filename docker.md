# Requirements 
    install docker, docker-compose

# Run dev enviroment
    at first time run
    source environment/compose/prebuild.sh


# Build images
    source environment/compose/dev.sh && docker-compose build %container_name%

# Run dev mode
    source environment/compose/dev.sh && docker-compose up -d %container_name%

# Run prod mode
    source environment/compose/prod.sh && docker-compose up -d

# Logs 
    source environment/compose/dev.sh && docker-compose logs -tf %container_name%
