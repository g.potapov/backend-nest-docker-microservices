const http = require("http");

const PORT = 38414;

function create() {
    const server = http.createServer(async (req, res) => {

        const { title, someText } = await parseBody(req);

        let html = "<html>";
            html += "<head>";
            html += "<title>test generate</title>";
            html += "</head>";
            html += "<body>";
            html += `<h3>${title}</h3>`;
            html += `<p>${someText}</p>`;
            html += "</body>";
            html += "</html>";

        res.write(html);
        res.end();
    });

    return server;
}

function parseBody(req) {
    return new Promise((resolve, _) => {
        let body = '';

        req.on('data', chunk => {
            body += chunk.toString();
        });
        req.on('end', () => {
            resolve(JSON.parse(body));
        });
    });
}


function run() {
    const server = create();
    server.listen(PORT, () => console.log("succesful connected!"));
    server.on("error", (err) => console.log(err));
}

run();