SERVICE=$1

if [ "$SERVICE" ]; then
    export COMPOSE_FILE=docker-compose.yaml:docker-compose.$SERVICE.dev-watch.yaml
else
    export COMPOSE_FILE=docker-compose.yaml
fi

export BACKEND_ENV=dev
export MOCK_RENDER_SERVICE=http://mock_render_client:38414
export MAILER_SERVICE=http://mailer:29265
